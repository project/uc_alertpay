UC_ALERTPAY MODULE FOR UBERCART PAYMENT README

uc_alertpay module adds the Alert Pay payment gateway integration to 
Ubercart's payment options.

You can create an alert pay account at https://www.alertpay.com

Enable this module and proceed to the ubercart configuration section for payments.
In the list of payment methods, enable Alert Pay as a payment method and enter 
the Alert Pay account details like 'Email of Merchant Account at AlertPay' and 
'Security Code of Merchant Account at AlertPay'.

To get these from your Alert Pay account, login to your account on 
https://www.alertpay.com
Proceed to the Business Tools section. Click on the API setup link on that page.
You can generate a new API Password, enable/disable payments and enable test modes.

For further information on the payment gateway visit https://www.alertpay.com

Releases may be downloaded at http://drupal.org/project/uc_alertpay.

