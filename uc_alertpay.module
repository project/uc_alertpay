<?php

/**
 * @file
 * Process payments using AlertPay.
 *
 * Ubercart payment gateways integration with AlertPay module sponsored by Blisstering Solutions - http://www.blisstering.com
 */

/**
 * Implementation of hook_menu().
 */
function uc_alertpay_menu() {
  // Handles a successful transaction from AlertPay
  $items['alertpay/complete'] = array(
    'title' => 'AlertPay payment complete',
    'page callback' => 'uc_alertpay_complete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  // Handles a canceled transaction from AlertPay
  $items['alpertpay/cancel'] = array(
    'title' => 'AlertPay payment cancelled',
    'page callback' => 'uc_alertpay_cancel',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_form_alter().
 *
 */
function uc_alertpay_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'alertpay') {
      $shipping = 0;
      foreach ($order->line_items as $item) {
        if ($item['type'] == 'shipping') {
          $shipping += $item['amount'];
        }
      }

      $tax = 0;
      if (module_exists('uc_taxes')) {
        foreach (uc_taxes_calculate($order) as $tax_item) {
          $tax += $tax_item->amount;
        }
      }

      $order_description = array();
      foreach ((array) $order->products as $product) {
        $order_description[] = $product->qty .'x '. $product->model;
      }

      $context = array(
        'revision' => 'formatted-original',
        'type' => 'amount',
      );
      $options = array(
        'sign' => FALSE,
        'thou' => FALSE,
        'dec' => '.',
      );

      $data = array(
        'ap_purchasetype' => 'item-goods',
        'ap_merchant' => variable_get('uc_alertpay_api_email', ''),
        'ap_currency' => variable_get('uc_currency_code', 'USD'),
        'ap_itemcode' => $order->order_id,
        'ap_itemname' => 'Shopping Cart Items',
        'ap_description' => drupal_substr(implode(', ', $order_description), 0, 255),
        'ap_quantity' => '1',
        'ap_amount' => uc_price($order->order_total - $shipping - $tax, $context, $options),
        'ap_returnurl' => url('alertpay/complete', array('absolute' => TRUE)),
        'ap_cancelurl' => url(variable_get('uc_alertpay_cancel_return_url', 'cart'), array('absolute' => TRUE)),
      );

      foreach ($data as $name => $value) {
        $_POST[$name] = $value;
      }
      $redirect_url = str_replace('&amp;', '&', variable_get('uc_alertpay_api_post_url', 'https://www.alertpay.com/PayProcess.aspx') . '?' . http_build_query($data));
      $form['#redirect'] = $redirect_url;
    }
  }

  if ($form_id == 'uc_cart_checkout_form') {
    $form['#submit'][] = 'uc_alertpay_checkout';
  }

  if ($form_id == 'uc_cart_checkout_review_form') {
    $form['#submit'][] = 'uc_alertpay_submit_form_submit';
  }
}

/**
 * Implementation of hook_payment_method().
 */
function uc_alertpay_payment_method() {
  $title = '<span><img src="https://www.alertpay.com/images/ap_logo.gif" style="position: relative; top: 5px; margin-right: 4px;"></img></span>';

  $methods[] = array(
    'id' => 'alertpay',
    'name' => t('AlertPay'),
    'title' => $title,
    'desc' => t('Redirect users to submit payments through AlertPay.'),
    'callback' => 'uc_payment_method_alertpay',
    'weight' => 7,
    'checkout' => FALSE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

// Handles the AlertPay payment method.
function uc_payment_method_alertpay($op, &$arg1) {
  switch ($op) {
    case 'settings':
      $form['uc_alertpay_api_post_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Post URL of AlertPay'),
        '#default_value' => variable_get('uc_alertpay_api_post_url', 'https://www.alertpay.com/PayProcess.aspx'),
      );
      $form['uc_alertpay_api_email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email of Merchant Account at AlertPay'),
        '#default_value' => variable_get('uc_alertpay_api_email', ''),
      );
      $form['uc_alertpay_cancel_return_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Cancel return URL'),
        '#description' => t('Specify the path customers who cancel their AlertPay payment will be directed to when they return to your site.'),
        '#default_value' => variable_get('uc_alertpay_cancel_return_url', 'cart'),
        '#size' => 32,
        '#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
      );
      $form['uc_alertpay_api_security_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Security Code of Merchant Account at AlertPay'),
        '#default_value' => variable_get('uc_alertpay_api_security_code', ''),
      );
      return $form;
  }
}

// Modify the form
function uc_alertpay_checkout_review_form($form_state, $order) {
  $shipping = 0;
  foreach ($order->line_items as $item) {
    if ($item['type'] == 'shipping') {
      $shipping += $item['amount'];
    }
  }

  $tax = 0;
  if (module_exists('uc_taxes')) {
    foreach (uc_taxes_calculate($order) as $tax_item) {
      $tax += $tax_item->amount;
    }
  }

  $order_description = array();
  foreach ((array) $order->products as $product) {
    $order_description[] = $product->qty .'x '. $product->model;
  }

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );
  $options = array(
    'sign' => FALSE,
    'thou' => FALSE,
    'dec' => '.',
  );

  $data = array(
    'ap_purchasetype' => 'item-goods',
    'ap_merchant' => variable_get('uc_alertpay_api_email', ''),
    'ap_currency' => variable_get('uc_currency_code', 'USD'),
    'ap_itemcode' => $order->order_id,
    'ap_itemname' => 'Shopping Cart Items',
    'ap_description' => drupal_substr(implode(', ', $order_description), 0, 255),
    'ap_quantity' => '1',
    'ap_amount' => uc_price($order->order_total - $shipping - $tax, $context, $options),
    'ap_returnurl' => url('alertpay/complete', array('absolute' => TRUE)),
    'ap_cancelurl' => url(variable_get('uc_alertpay_cancel_return_url', 'cart'), array('absolute' => TRUE)),
  );

  $get_variables = '';
  foreach ($data as $name => $value) {
    $get_variables .= $name . '=' . $value;
  }

  $form['#action'] = variable_get('uc_alertpay_api_post_url', 'https://www.alertpay.com/PayProcess.aspx') . '&' . http_build_query($data);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Goto AlertPay'),
  );
}

// Handles a successful transaction from AlertPay
function uc_alertpay_complete($order_id = 0) {
  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }

  if (!($order = uc_order_load($_SESSION['cart_order']))) {
    drupal_goto('cart');
  }

  $site_security_code = variable_get('uc_alertpay_api_security_code', '');
  $ap_SecurityCode = $_POST['ap_securitycode'];
  $ap_Merchant = $_POST['ap_merchant'];
  $ap_Status = $_POST['ap_status'];
  $ap_ItemCode = $_POST['ap_itemcode'];
  $ap_Test = $_POST['ap_test'];
  if ($site_security_code == '') {
    drupal_set_message(t('The administrator has not verified the security code.'));
    drupal_goto('');
  }
  elseif ($site_security_code == $ap_SecurityCode) {
    if ($ap_Merchant == variable_get('uc_alertpay_api_email', '')) {
      if (intval($_SESSION['cart_order']) != $ap_ItemCode) {
        $_SESSION['cart_order'] = $ap_ItemCode;
      }

      if (!($order = uc_order_load($ap_ItemCode))) {
        drupal_goto('cart');
      }

      if ($ap_Status == 'Success') {

        // This lets us know it's a legitimate access of transaction being successful
        $_SESSION['do_complete'] = TRUE;

        drupal_goto('cart/checkout/complete');
      }
      else {
        // The transaction was not complete

      }
    }
    else {
      drupal_set_message(t('The email address did not match with the administrator\'s merchant account. We are therefore unable to process the request.'));
      drupal_goto('');
    }
  }
  else {
    drupal_set_message(t('The security code did not match with the administrator\'s merchant account. We are therefore unable to process the request.'));
    drupal_goto('');
  }
}

// Handles a canceled transaction from AlertPay
function uc_alertpay_cancel() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Your AlertPay payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));

  drupal_goto(variable_get('uc_alertpay_cancel_return_url', 'cart'));
}

/**
 * Implementation of hook_order().
 */

function uc_alertpay_order($op, &$arg1, $arg2) {
  $order    = $arg1;
  $payment_method = drupal_strtolower($order->payment_method);
  switch ($op) {
    case 'submit':
      // fires when the order is submitted
      if ($payment_method == 'alertpay') {
        //uc_userpoints_payment_payment($order);
      }
      break;
    case 'update':
      // if the order is canceled we need to refund thier points because we are too lazy to do it by hand...
      if ($arg2 == 'canceled') {
        if (drupal_strtolower($order->payment_method) == 'alertpay') {
          //uc_userpoints_payment_refund($order);
        }
      }
      break;
    case 'delete':
      if (drupal_strtolower($order->payment_method) == 'alertpay') {
        //uc_userpoints_payment_refund($order);
      }
      break;
    case 'can_delete';
    return FALSE;
    break;
  }
}

function uc_alertpay_payment($order) {
  global $user;

}